package br.ufc.quixada.cc.poo.principais;

import br.ufc.quixada.cc.poo.interfaces.Imprimivel;
import br.ufc.quixada.cc.poo.modelo.Passagem;

public class PrincipalImprimivel implements Imprimivel{
    public static void main (String[] args){
        
    }

    @Override
    public void mostrarPassagem() {
        System.out.println(Passagem.class.toString());
    }
}
