package br.ufc.quixada.cc.poo.modelo;

public class Vendedor extends Funcionario{
    private int cargaHoraria;

    public Vendedor() {
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    @Override
    public String toString() {
        return "Vendedor{" + "cargaHoraria=" + cargaHoraria + '}';
    }
    
    public void realizarVenda(Onibus o, Passagem p){
        /*
        *
        *
        */
        darBonificacao();
    }

    @Override
    public void darBonificacao() {
        this.setSalario(this.getSalario() + 3);
    }
}
