package br.ufc.quixada.cc.poo.modelo;

import br.ufc.quixada.cc.poo.interfaces.Imprimivel;
import java.util.List;

public class Onibus implements Imprimivel{
    private int codOnibus;
    private Motorista motorista;
    private List<Passagem> passagens;
    private List<Cliente> passageiros;

    public Onibus() {
    }

    public int getCodOnibus() {
        return codOnibus;
    }

    public void setCodOnibus(int codOnibus) {
        this.codOnibus = codOnibus;
    }

    public Motorista getMotorista() {
        return motorista;
    }

    public void setMotorista(Motorista motorista) {
        this.motorista = motorista;
    }

    public List<Passagem> getPassagens() {
        return passagens;
    }

    public void setPassagens(List<Passagem> passagens) {
        this.passagens = passagens;
    }

    public List<Cliente> getPassageiros() {
        return passageiros;
    }

    public void setPassageiros(List<Cliente> passageiros) {
        this.passageiros = passageiros;
    }

    @Override
    public String toString() {
        return "Onibus{" + "codOnibus=" + codOnibus + ", motorista=" + motorista + ", passagens=" + passagens + ", passageiros=" + passageiros + '}';
    }
    
    public void adicionarPassagemOnibus(Passagem p){
    }
    
    @Override
    public void mostrarPassagem() {
        System.out.println(Passagem.class.toString());
    }
    
}
