package br.ufc.quixada.cc.poo.modelo;

import java.time.LocalDate;

public class ClientePessoaJuridica extends Cliente{
    private String cnpj;
    private LocalDate dataAbertura;

    public ClientePessoaJuridica() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public LocalDate getDataAbertura() {
        return dataAbertura;
    }

    public void setDataAbertura(LocalDate dataAbertura) {
        this.dataAbertura = dataAbertura;
    }

    @Override
    public String toString() {
        return "ClientePessoaJuridica{" + "cnpj=" + cnpj + ", dataAbertura=" + dataAbertura + '}';
    }
    
    
}
