package br.ufc.quixada.cc.poo.modelo;

public abstract class Cliente extends Pessoa{
    private String endereco;

    public Cliente() {
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    @Override
    public String toString() {
        return "Cliente{" + "endereco=" + endereco + '}';
    }
    
    
}
