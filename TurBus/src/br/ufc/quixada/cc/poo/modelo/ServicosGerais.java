package br.ufc.quixada.cc.poo.modelo;

public class ServicosGerais extends Funcionario{
    private int tempoServiço;

    public ServicosGerais() {
    }

    public int getTempoServiço() {
        return tempoServiço;
    }

    public void setTempoServiço(int tempoServiço) {
        this.tempoServiço = tempoServiço;
    }

    @Override
    public String toString() {
        return "ServicosGerais{" + "tempoServi\u00e7o=" + tempoServiço + '}';
    }
    
    public void limpar(){
        darBonificacao();
    }

    @Override
    public void darBonificacao() {
        this.setSalario(this.getSalario() + 3);
    }
}
