package br.ufc.quixada.cc.poo.modelo;

public class Motorista extends Funcionario{
    private String cnh;

    public Motorista() {
    }

    public String getCnh() {
        return cnh;
    }

    public void setCnh(String cnh) {
        this.cnh = cnh;
    }

    @Override
    public String toString() {
        return "Motorista{" + "cnh=" + cnh + '}';
    }

    @Override
    public void darBonificacao() {
        /*
        *
        *
        */
        this.setSalario((this.getSalario() * 5) / 100);
    }
    
    public void realizarViagem(){
        darBonificacao();
    }
    
}
