package uber;

public class Motorista {
    private String nome;
    private String cnh;
    private String placa;
    private float nota;

    public Motorista(String nome, String cnh, String placa, float nota) {
        this.nome = nome;
        this.cnh = cnh;
        this.placa = placa;
        this.nota = nota;
    }

    @Override
    public String toString() {
        return "Motorista{" + "nome=" + nome + ", cnh=" + cnh + ", placa=" + placa + ", nota=" + nota + '}';
    }

    public String getNome() {
        return nome;
    }

    public String getCnh() {
        return cnh;
    }

    public String getPlaca() {
        return placa;
    }

    public float getNota() {
        return nota;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void setCnh(String cnh){
        this.cnh = cnh;
    }
    
    public void setPlaca(String placa){
        this.placa = placa;
    }
    
    public void setNota(float nota){
        this.nota = nota;
    }
    
    public void realizarCorrida( Cliente c, Corrida cor){
        System.out.println("Cliente:" + c.getNome() + " " + "Motorista " + getNome() + " " + "Nota Mot: " + getNota() + " " + "Local partida: " + cor.getPartida() + " " + "Local destino: " + cor.getDestino());
    }
}
